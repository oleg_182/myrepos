package app;

import java.io.File;
import java.io.IOException;

import java.util.*;


import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;



public class Application {
    public static void main(String[] args) {

        ObjectMapper mapper = new ObjectMapper();
        try {
            File json = new File("src/events.json");
            mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
            List<Event> map = new ObjectMapper().readValue(json, new TypeReference<List<Event>>() {});
            Collections.sort(map, Comparator.comparing(Event::getTime));
            System.out.println(mapper.writeValueAsString(map));
        } catch (JsonGenerationException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}




